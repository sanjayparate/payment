<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PaymentMethodGateway */

$this->title = Yii::t('app', 'Create Payment Method Gateway');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Payment Method Gateways'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-method-gateway-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
