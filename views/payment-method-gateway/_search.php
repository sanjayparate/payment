<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PaymentMethodGatewaySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-method-gateway-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'iRawId') ?>

    <?= $form->field($model, 'sCurrencyCode') ?>

    <?= $form->field($model, 'iPaymentMethodId') ?>

    <?= $form->field($model, 'iPaymentGatewayId') ?>

    <?= $form->field($model, 'bIsActive') ?>

    <?php // echo $form->field($model, 'sEntityCode') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
