<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PaymentMethodGateway */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-method-gateway-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'sCurrencyCode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'iPaymentMethodId')->textInput() ?>

    <?= $form->field($model, 'iPaymentGatewayId')->textInput() ?>

    <?= $form->field($model, 'bIsActive')->textInput() ?>

    <?= $form->field($model, 'sEntityCode')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
