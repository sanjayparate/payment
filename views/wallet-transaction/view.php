<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\WalletTransaction */

$this->title = $model->iRawId;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Wallet Transactions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wallet-transaction-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->iRawId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->iRawId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'iRawId',
            'iWalletId',
            'iEntityRoleId',
            'sEntityCode',
            'sTransactionType',
            'nTransactionAmount',
            'sCurrencyCode',
            'nBalanceBeforeTransaction',
            'nBalanceAfterTransaction',
            'nBalanceUpdateTime',
            'iPaymentMethodId',
            'sIntegrationReferenceId',
            'sIntegrationRequest:ntext',
            'sIntegrationResponse:ntext',
            'iPaymentStatusId',
            'sRemarks:ntext',
            'iPaymentGatewayId',
            'iDeviceRegistrationId',
            'sDeviceOs',
            'sDeviceOsVersion',
            'sDeviceMake',
            'sDeviceModel',
            'sApplicationType',
            'sApplicationVersion',
            'dCreatedDateTime',
            'dModifiedDateTime',
        ],
    ]) ?>

</div>
