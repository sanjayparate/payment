<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\WalletTransaction */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="wallet-transaction-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'iRawId')->textInput() ?>

    <?= $form->field($model, 'iWalletId')->textInput() ?>

    <?= $form->field($model, 'iEntityRoleId')->textInput() ?>

    <?= $form->field($model, 'sEntityCode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sTransactionType')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nTransactionAmount')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sCurrencyCode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nBalanceBeforeTransaction')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nBalanceAfterTransaction')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nBalanceUpdateTime')->textInput() ?>

    <?= $form->field($model, 'iPaymentMethodId')->textInput() ?>

    <?= $form->field($model, 'sIntegrationReferenceId')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sIntegrationRequest')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'sIntegrationResponse')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'iPaymentStatusId')->textInput() ?>

    <?= $form->field($model, 'sRemarks')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'iPaymentGatewayId')->textInput() ?>

    <?= $form->field($model, 'iDeviceRegistrationId')->textInput() ?>

    <?= $form->field($model, 'sDeviceOs')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sDeviceOsVersion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sDeviceMake')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sDeviceModel')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sApplicationType')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sApplicationVersion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dCreatedDateTime')->textInput() ?>

    <?= $form->field($model, 'dModifiedDateTime')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
