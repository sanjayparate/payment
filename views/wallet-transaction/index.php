<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\WalletTransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Wallet Transactions');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wallet-transaction-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Wallet Transaction'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'iRawId',
            'iWalletId',
            'iEntityRoleId',
            'sEntityCode',
            'sTransactionType',
            // 'nTransactionAmount',
            // 'sCurrencyCode',
            // 'nBalanceBeforeTransaction',
            // 'nBalanceAfterTransaction',
            // 'nBalanceUpdateTime',
            // 'iPaymentMethodId',
            // 'sIntegrationReferenceId',
            // 'sIntegrationRequest:ntext',
            // 'sIntegrationResponse:ntext',
            // 'iPaymentStatusId',
            // 'sRemarks:ntext',
            // 'iPaymentGatewayId',
            // 'iDeviceRegistrationId',
            // 'sDeviceOs',
            // 'sDeviceOsVersion',
            // 'sDeviceMake',
            // 'sDeviceModel',
            // 'sApplicationType',
            // 'sApplicationVersion',
            // 'dCreatedDateTime',
            // 'dModifiedDateTime',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
