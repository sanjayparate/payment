<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\WalletTransactionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="wallet-transaction-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'iRawId') ?>

    <?= $form->field($model, 'iWalletId') ?>

    <?= $form->field($model, 'iEntityRoleId') ?>

    <?= $form->field($model, 'sEntityCode') ?>

    <?= $form->field($model, 'sTransactionType') ?>

    <?php // echo $form->field($model, 'nTransactionAmount') ?>

    <?php // echo $form->field($model, 'sCurrencyCode') ?>

    <?php // echo $form->field($model, 'nBalanceBeforeTransaction') ?>

    <?php // echo $form->field($model, 'nBalanceAfterTransaction') ?>

    <?php // echo $form->field($model, 'nBalanceUpdateTime') ?>

    <?php // echo $form->field($model, 'iPaymentMethodId') ?>

    <?php // echo $form->field($model, 'sIntegrationReferenceId') ?>

    <?php // echo $form->field($model, 'sIntegrationRequest') ?>

    <?php // echo $form->field($model, 'sIntegrationResponse') ?>

    <?php // echo $form->field($model, 'iPaymentStatusId') ?>

    <?php // echo $form->field($model, 'sRemarks') ?>

    <?php // echo $form->field($model, 'iPaymentGatewayId') ?>

    <?php // echo $form->field($model, 'iDeviceRegistrationId') ?>

    <?php // echo $form->field($model, 'sDeviceOs') ?>

    <?php // echo $form->field($model, 'sDeviceOsVersion') ?>

    <?php // echo $form->field($model, 'sDeviceMake') ?>

    <?php // echo $form->field($model, 'sDeviceModel') ?>

    <?php // echo $form->field($model, 'sApplicationType') ?>

    <?php // echo $form->field($model, 'sApplicationVersion') ?>

    <?php // echo $form->field($model, 'dCreatedDateTime') ?>

    <?php // echo $form->field($model, 'dModifiedDateTime') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
