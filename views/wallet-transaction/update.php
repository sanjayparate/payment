<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\WalletTransaction */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Wallet Transaction',
]) . $model->iRawId;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Wallet Transactions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->iRawId, 'url' => ['view', 'id' => $model->iRawId]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="wallet-transaction-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
