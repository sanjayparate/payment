<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PaymentStatus */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Payment Status',
]) . $model->iPaymentStatusId;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Payment Statuses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->iPaymentStatusId, 'url' => ['view', 'id' => $model->iPaymentStatusId]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="payment-status-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
