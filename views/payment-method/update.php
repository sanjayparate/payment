<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PaymentMethod */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Payment Method',
]) . $model->iPaymentMethodId;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Payment Methods'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->iPaymentMethodId, 'url' => ['view', 'id' => $model->iPaymentMethodId]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="payment-method-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
