<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CustauthSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="custauth-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'mobile') ?>

    <?= $form->field($model, 'sMobileNo2') ?>

    <?= $form->field($model, 'password') ?>

    <?= $form->field($model, 'name') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'sEmailId2') ?>

    <?php // echo $form->field($model, 'bIsACPreferred') ?>

    <?php // echo $form->field($model, 'sClientStatusCode') ?>

    <?php // echo $form->field($model, 'iDeviceType') ?>

    <?php // echo $form->field($model, 'model') ?>

    <?php // echo $form->field($model, 'deviceId') ?>

    <?php // echo $form->field($model, 'tGCMRegistrationNo') ?>

    <?php // echo $form->field($model, 'version') ?>

    <?php // echo $form->field($model, 'networkOperator') ?>

    <?php // echo $form->field($model, 'createddate') ?>

    <?php // echo $form->field($model, 'lastupdate') ?>

    <?php // echo $form->field($model, 'sMobileVerificationCode') ?>

    <?php // echo $form->field($model, 'bIsMobileVerified') ?>

    <?php // echo $form->field($model, 'dMobileVerificationTime') ?>

    <?php // echo $form->field($model, 'bIsEmailVerified') ?>

    <?php // echo $form->field($model, 'sCountryCode') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
