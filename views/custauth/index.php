<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\CustauthSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Custauths');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="custauth-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Custauth'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'mobile',
            'sMobileNo2',
            'password',
            'name',
            // 'email:email',
            // 'sEmailId2:email',
            // 'bIsACPreferred',
            // 'sClientStatusCode',
            // 'iDeviceType',
            // 'model',
            // 'deviceId',
            // 'tGCMRegistrationNo',
            // 'version',
            // 'networkOperator',
            // 'createddate',
            // 'lastupdate',
            // 'sMobileVerificationCode',
            // 'bIsMobileVerified',
            // 'dMobileVerificationTime',
            // 'bIsEmailVerified:email',
            // 'sCountryCode',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
