<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Custauth */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Custauths'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="custauth-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'mobile',
            'sMobileNo2',
            'password',
            'name',
            'email:email',
            'sEmailId2:email',
            'bIsACPreferred',
            'sClientStatusCode',
            'iDeviceType',
            'model',
            'deviceId',
            'tGCMRegistrationNo',
            'version',
            'networkOperator',
            'createddate',
            'lastupdate',
            'sMobileVerificationCode',
            'bIsMobileVerified',
            'dMobileVerificationTime',
            'bIsEmailVerified:email',
            'sCountryCode',
        ],
    ]) ?>

</div>
