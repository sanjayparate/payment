<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Custauth */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="custauth-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'mobile')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sMobileNo2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sEmailId2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bIsACPreferred')->textInput() ?>

    <?= $form->field($model, 'sClientStatusCode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'iDeviceType')->textInput() ?>

    <?= $form->field($model, 'model')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'deviceId')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tGCMRegistrationNo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'version')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'networkOperator')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'createddate')->textInput() ?>

    <?= $form->field($model, 'lastupdate')->textInput() ?>

    <?= $form->field($model, 'sMobileVerificationCode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bIsMobileVerified')->textInput() ?>

    <?= $form->field($model, 'dMobileVerificationTime')->textInput() ?>

    <?= $form->field($model, 'bIsEmailVerified')->textInput() ?>

    <?= $form->field($model, 'sCountryCode')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
