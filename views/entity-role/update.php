<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EntityRole */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Entity Role',
]) . $model->sEntityCode;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Entity Roles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->sEntityCode, 'url' => ['view', 'id' => $model->sEntityCode]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="entity-role-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
