<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Wallet */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="wallet-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'iWalletId')->textInput() ?>

    <?= $form->field($model, 'iEntityRoleId')->textInput() ?>

    <?= $form->field($model, 'sEntityCode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sCurrencyCode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nBalance')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bIsActive')->textInput() ?>

    <?= $form->field($model, 'dCreatedDateTime')->textInput() ?>

    <?= $form->field($model, 'dModifiedDateTime')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
