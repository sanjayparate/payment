<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\WalletSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="wallet-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'iWalletId') ?>

    <?= $form->field($model, 'iEntityRoleId') ?>

    <?= $form->field($model, 'sEntityCode') ?>

    <?= $form->field($model, 'sCurrencyCode') ?>

    <?= $form->field($model, 'nBalance') ?>

    <?php // echo $form->field($model, 'bIsActive') ?>

    <?php // echo $form->field($model, 'dCreatedDateTime') ?>

    <?php // echo $form->field($model, 'dModifiedDateTime') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
