<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PaymentGateway */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Payment Gateway',
]) . $model->iPaymentGatewayId;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Payment Gateways'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->iPaymentGatewayId, 'url' => ['view', 'id' => $model->iPaymentGatewayId]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="payment-gateway-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
