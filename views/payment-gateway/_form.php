<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PaymentGateway */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-gateway-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'sPaymentGatewayName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bIsActive')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
