<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\DriverSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Drivers');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="driver-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Driver'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'iSubscriberId',
            'mobile',
            'sPrimaryMobileNo',
            'iCabOperatorId',
            'iCityId',
            // 'vehicle_registration_no',
            // 'driver_name',
            // 'iDestinationLocId',
            // 'iDestinationZoneId',
            // 'iDestinationLat',
            // 'iDestinationLong',
            // 'end_time',
            // 'latitude',
            // 'longitude',
            // 'sTowerAddress',
            // 'nDistanceFromNearestLocation',
            // 'location_id',
            // 'zone_id',
            // 'cell_id_update_time',
            // 'self_update',
            // 'time_self_update',
            // 'balance',
            // 'credit_limit',
            // 'blocked',
            // 'balance_update_datetime',
            // 'update_method',
            // 'last_update_time',
            // 'iFareMeterStatus',
            // 'iTodayRevenue',
            // 'iDailyTargetRevenue',
            // 'iTodayDeficitRevenue',
            // 'iTodayTripCount',
            // 'iDailyTargetTripCount',
            // 'iTodayDeficitTripCount',
            // 'bIsIDEACallAPIToBeUsed',
            // 'bIsLockedForOBDType1Call',
            // 'vehicle_type',
            // 'iVehicleTypeId',
            // 'sVehicleModelCode',
            // 'device_tag',
            // 'bHasVehicleCarrier',
            // 'bIsACVehicle',
            // 'bPrefForNonACTrip',
            // 'owner_phone',
            // 'taxi_stand',
            // 'rating',
            // 'remarks',
            // 'bIsNightDriver',
            // 'tShiftFromTime',
            // 'tshiftToTime',
            // 'iAcquirerUserId',
            // 'dDateOfAcquisition',
            // 'sPermitHolderName',
            // 'sPermitNumber',
            // 'dDateOfRegistration',
            // 'dLastSettlementDate',
            // 'bIsDisplay',
            // 'bIsDeleted',
            // 'sDeleteReason',
            // 'iCreatedUserId',
            // 'dCreatedDateTime',
            // 'iLastModifiedUserId',
            // 'dLastModifiedDateTime',
            // 'iDeleteUserId',
            // 'dDeleteDateTime',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
