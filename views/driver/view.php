<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Driver */

$this->title = $model->iSubscriberId;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Drivers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="driver-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->iSubscriberId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->iSubscriberId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'iSubscriberId',
            'mobile',
            'sPrimaryMobileNo',
            'iCabOperatorId',
            'iCityId',
            'vehicle_registration_no',
            'driver_name',
            'iDestinationLocId',
            'iDestinationZoneId',
            'iDestinationLat',
            'iDestinationLong',
            'end_time',
            'latitude',
            'longitude',
            'sTowerAddress',
            'nDistanceFromNearestLocation',
            'location_id',
            'zone_id',
            'cell_id_update_time',
            'self_update',
            'time_self_update',
            'balance',
            'credit_limit',
            'blocked',
            'balance_update_datetime',
            'update_method',
            'last_update_time',
            'iFareMeterStatus',
            'iTodayRevenue',
            'iDailyTargetRevenue',
            'iTodayDeficitRevenue',
            'iTodayTripCount',
            'iDailyTargetTripCount',
            'iTodayDeficitTripCount',
            'bIsIDEACallAPIToBeUsed',
            'bIsLockedForOBDType1Call',
            'vehicle_type',
            'iVehicleTypeId',
            'sVehicleModelCode',
            'device_tag',
            'bHasVehicleCarrier',
            'bIsACVehicle',
            'bPrefForNonACTrip',
            'owner_phone',
            'taxi_stand',
            'rating',
            'remarks',
            'bIsNightDriver',
            'tShiftFromTime',
            'tshiftToTime',
            'iAcquirerUserId',
            'dDateOfAcquisition',
            'sPermitHolderName',
            'sPermitNumber',
            'dDateOfRegistration',
            'dLastSettlementDate',
            'bIsDisplay',
            'bIsDeleted',
            'sDeleteReason',
            'iCreatedUserId',
            'dCreatedDateTime',
            'iLastModifiedUserId',
            'dLastModifiedDateTime',
            'iDeleteUserId',
            'dDeleteDateTime',
        ],
    ]) ?>

</div>
