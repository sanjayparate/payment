<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Driver */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="driver-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'mobile')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sPrimaryMobileNo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'iCabOperatorId')->textInput() ?>

    <?= $form->field($model, 'iCityId')->textInput() ?>

    <?= $form->field($model, 'vehicle_registration_no')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'driver_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'iDestinationLocId')->textInput() ?>

    <?= $form->field($model, 'iDestinationZoneId')->textInput() ?>

    <?= $form->field($model, 'iDestinationLat')->textInput() ?>

    <?= $form->field($model, 'iDestinationLong')->textInput() ?>

    <?= $form->field($model, 'end_time')->textInput() ?>

    <?= $form->field($model, 'latitude')->textInput() ?>

    <?= $form->field($model, 'longitude')->textInput() ?>

    <?= $form->field($model, 'sTowerAddress')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nDistanceFromNearestLocation')->textInput() ?>

    <?= $form->field($model, 'location_id')->textInput() ?>

    <?= $form->field($model, 'zone_id')->textInput() ?>

    <?= $form->field($model, 'cell_id_update_time')->textInput() ?>

    <?= $form->field($model, 'self_update')->textInput() ?>

    <?= $form->field($model, 'time_self_update')->textInput() ?>

    <?= $form->field($model, 'balance')->textInput() ?>

    <?= $form->field($model, 'credit_limit')->textInput() ?>

    <?= $form->field($model, 'blocked')->textInput() ?>

    <?= $form->field($model, 'balance_update_datetime')->textInput() ?>

    <?= $form->field($model, 'update_method')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'last_update_time')->textInput() ?>

    <?= $form->field($model, 'iFareMeterStatus')->textInput() ?>

    <?= $form->field($model, 'iTodayRevenue')->textInput() ?>

    <?= $form->field($model, 'iDailyTargetRevenue')->textInput() ?>

    <?= $form->field($model, 'iTodayDeficitRevenue')->textInput() ?>

    <?= $form->field($model, 'iTodayTripCount')->textInput() ?>

    <?= $form->field($model, 'iDailyTargetTripCount')->textInput() ?>

    <?= $form->field($model, 'iTodayDeficitTripCount')->textInput() ?>

    <?= $form->field($model, 'bIsIDEACallAPIToBeUsed')->textInput() ?>

    <?= $form->field($model, 'bIsLockedForOBDType1Call')->textInput() ?>

    <?= $form->field($model, 'vehicle_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'iVehicleTypeId')->textInput() ?>

    <?= $form->field($model, 'sVehicleModelCode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'device_tag')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bHasVehicleCarrier')->textInput() ?>

    <?= $form->field($model, 'bIsACVehicle')->textInput() ?>

    <?= $form->field($model, 'bPrefForNonACTrip')->textInput() ?>

    <?= $form->field($model, 'owner_phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'taxi_stand')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rating')->textInput() ?>

    <?= $form->field($model, 'remarks')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bIsNightDriver')->textInput() ?>

    <?= $form->field($model, 'tShiftFromTime')->textInput() ?>

    <?= $form->field($model, 'tshiftToTime')->textInput() ?>

    <?= $form->field($model, 'iAcquirerUserId')->textInput() ?>

    <?= $form->field($model, 'dDateOfAcquisition')->textInput() ?>

    <?= $form->field($model, 'sPermitHolderName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sPermitNumber')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dDateOfRegistration')->textInput() ?>

    <?= $form->field($model, 'dLastSettlementDate')->textInput() ?>

    <?= $form->field($model, 'bIsDisplay')->textInput() ?>

    <?= $form->field($model, 'bIsDeleted')->textInput() ?>

    <?= $form->field($model, 'sDeleteReason')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'iCreatedUserId')->textInput() ?>

    <?= $form->field($model, 'dCreatedDateTime')->textInput() ?>

    <?= $form->field($model, 'iLastModifiedUserId')->textInput() ?>

    <?= $form->field($model, 'dLastModifiedDateTime')->textInput() ?>

    <?= $form->field($model, 'iDeleteUserId')->textInput() ?>

    <?= $form->field($model, 'dDeleteDateTime')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
