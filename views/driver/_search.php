<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DriverSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="driver-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'iSubscriberId') ?>

    <?= $form->field($model, 'mobile') ?>

    <?= $form->field($model, 'sPrimaryMobileNo') ?>

    <?= $form->field($model, 'iCabOperatorId') ?>

    <?= $form->field($model, 'iCityId') ?>

    <?php // echo $form->field($model, 'vehicle_registration_no') ?>

    <?php // echo $form->field($model, 'driver_name') ?>

    <?php // echo $form->field($model, 'iDestinationLocId') ?>

    <?php // echo $form->field($model, 'iDestinationZoneId') ?>

    <?php // echo $form->field($model, 'iDestinationLat') ?>

    <?php // echo $form->field($model, 'iDestinationLong') ?>

    <?php // echo $form->field($model, 'end_time') ?>

    <?php // echo $form->field($model, 'latitude') ?>

    <?php // echo $form->field($model, 'longitude') ?>

    <?php // echo $form->field($model, 'sTowerAddress') ?>

    <?php // echo $form->field($model, 'nDistanceFromNearestLocation') ?>

    <?php // echo $form->field($model, 'location_id') ?>

    <?php // echo $form->field($model, 'zone_id') ?>

    <?php // echo $form->field($model, 'cell_id_update_time') ?>

    <?php // echo $form->field($model, 'self_update') ?>

    <?php // echo $form->field($model, 'time_self_update') ?>

    <?php // echo $form->field($model, 'balance') ?>

    <?php // echo $form->field($model, 'credit_limit') ?>

    <?php // echo $form->field($model, 'blocked') ?>

    <?php // echo $form->field($model, 'balance_update_datetime') ?>

    <?php // echo $form->field($model, 'update_method') ?>

    <?php // echo $form->field($model, 'last_update_time') ?>

    <?php // echo $form->field($model, 'iFareMeterStatus') ?>

    <?php // echo $form->field($model, 'iTodayRevenue') ?>

    <?php // echo $form->field($model, 'iDailyTargetRevenue') ?>

    <?php // echo $form->field($model, 'iTodayDeficitRevenue') ?>

    <?php // echo $form->field($model, 'iTodayTripCount') ?>

    <?php // echo $form->field($model, 'iDailyTargetTripCount') ?>

    <?php // echo $form->field($model, 'iTodayDeficitTripCount') ?>

    <?php // echo $form->field($model, 'bIsIDEACallAPIToBeUsed') ?>

    <?php // echo $form->field($model, 'bIsLockedForOBDType1Call') ?>

    <?php // echo $form->field($model, 'vehicle_type') ?>

    <?php // echo $form->field($model, 'iVehicleTypeId') ?>

    <?php // echo $form->field($model, 'sVehicleModelCode') ?>

    <?php // echo $form->field($model, 'device_tag') ?>

    <?php // echo $form->field($model, 'bHasVehicleCarrier') ?>

    <?php // echo $form->field($model, 'bIsACVehicle') ?>

    <?php // echo $form->field($model, 'bPrefForNonACTrip') ?>

    <?php // echo $form->field($model, 'owner_phone') ?>

    <?php // echo $form->field($model, 'taxi_stand') ?>

    <?php // echo $form->field($model, 'rating') ?>

    <?php // echo $form->field($model, 'remarks') ?>

    <?php // echo $form->field($model, 'bIsNightDriver') ?>

    <?php // echo $form->field($model, 'tShiftFromTime') ?>

    <?php // echo $form->field($model, 'tshiftToTime') ?>

    <?php // echo $form->field($model, 'iAcquirerUserId') ?>

    <?php // echo $form->field($model, 'dDateOfAcquisition') ?>

    <?php // echo $form->field($model, 'sPermitHolderName') ?>

    <?php // echo $form->field($model, 'sPermitNumber') ?>

    <?php // echo $form->field($model, 'dDateOfRegistration') ?>

    <?php // echo $form->field($model, 'dLastSettlementDate') ?>

    <?php // echo $form->field($model, 'bIsDisplay') ?>

    <?php // echo $form->field($model, 'bIsDeleted') ?>

    <?php // echo $form->field($model, 'sDeleteReason') ?>

    <?php // echo $form->field($model, 'iCreatedUserId') ?>

    <?php // echo $form->field($model, 'dCreatedDateTime') ?>

    <?php // echo $form->field($model, 'iLastModifiedUserId') ?>

    <?php // echo $form->field($model, 'dLastModifiedDateTime') ?>

    <?php // echo $form->field($model, 'iDeleteUserId') ?>

    <?php // echo $form->field($model, 'dDeleteDateTime') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
