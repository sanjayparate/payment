<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_wallet".
 *
 * @property integer $iWalletId
 * @property integer $iEntityRoleId
 * @property string $sEntityCode
 * @property string $sCurrencyCode
 * @property string $nBalance
 * @property integer $bIsActive
 * @property string $dCreatedDateTime
 * @property string $dModifiedDateTime
 */
class Wallet extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_wallet';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['iWalletId', 'iEntityRoleId', 'sEntityCode', 'sCurrencyCode', 'nBalance'], 'required'],
            [['iWalletId', 'iEntityRoleId', 'bIsActive'], 'integer'],
            [['nBalance'], 'number'],
            [['dCreatedDateTime', 'dModifiedDateTime'], 'safe'],
            [['sEntityCode'], 'string', 'max' => 4],
            [['sCurrencyCode'], 'string', 'max' => 5],
            [['iEntityRoleId', 'sEntityCode', 'sCurrencyCode'], 'unique', 'targetAttribute' => ['iEntityRoleId', 'sEntityCode', 'sCurrencyCode'], 'message' => 'The combination of I Entity Role ID, S Entity Code and S Currency Code has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'iWalletId' => 'I Wallet ID',
            'iEntityRoleId' => 'I Entity Role ID',
            'sEntityCode' => 'S Entity Code',
            'sCurrencyCode' => 'S Currency Code',
            'nBalance' => 'N Balance',
            'bIsActive' => 'B Is Active',
            'dCreatedDateTime' => 'D Created Date Time',
            'dModifiedDateTime' => 'D Modified Date Time',
        ];
    }
}
