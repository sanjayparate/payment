<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "custauth".
 *
 * @property integer $id
 * @property string $mobile
 * @property string $sMobileNo2
 * @property string $password
 * @property string $name
 * @property string $email
 * @property string $sEmailId2
 * @property integer $bIsACPreferred
 * @property string $sClientStatusCode
 * @property integer $iDeviceType
 * @property string $model
 * @property string $deviceId
 * @property string $tGCMRegistrationNo
 * @property string $version
 * @property string $networkOperator
 * @property string $createddate
 * @property string $lastupdate
 * @property string $sMobileVerificationCode
 * @property integer $bIsMobileVerified
 * @property string $dMobileVerificationTime
 * @property integer $bIsEmailVerified
 * @property string $sCountryCode
 */
class Custauth extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'custauth';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bIsACPreferred', 'iDeviceType', 'bIsMobileVerified', 'bIsEmailVerified'], 'integer'],
            [['createddate', 'lastupdate', 'dMobileVerificationTime'], 'safe'],
            [['mobile', 'sClientStatusCode'], 'string', 'max' => 20],
            [['sMobileNo2'], 'string', 'max' => 12],
            [['password', 'name', 'email', 'sEmailId2'], 'string', 'max' => 100],
            [['model', 'version', 'networkOperator'], 'string', 'max' => 200],
            [['deviceId', 'tGCMRegistrationNo'], 'string', 'max' => 500],
            [['sMobileVerificationCode'], 'string', 'max' => 6],
            [['sCountryCode'], 'string', 'max' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mobile' => 'Mobile',
            'sMobileNo2' => 'S Mobile No2',
            'password' => 'Password',
            'name' => 'Name',
            'email' => 'Email',
            'sEmailId2' => 'S Email Id2',
            'bIsACPreferred' => 'B Is Acpreferred',
            'sClientStatusCode' => 'S Client Status Code',
            'iDeviceType' => 'I Device Type',
            'model' => 'Model',
            'deviceId' => 'Device ID',
            'tGCMRegistrationNo' => 'T Gcmregistration No',
            'version' => 'Version',
            'networkOperator' => 'Network Operator',
            'createddate' => 'Createddate',
            'lastupdate' => 'Lastupdate',
            'sMobileVerificationCode' => 'S Mobile Verification Code',
            'bIsMobileVerified' => 'B Is Mobile Verified',
            'dMobileVerificationTime' => 'D Mobile Verification Time',
            'bIsEmailVerified' => 'B Is Email Verified',
            'sCountryCode' => 'S Country Code',
        ];
    }
}
