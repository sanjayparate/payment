<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_mst_payment_method".
 *
 * @property integer $iPaymentMethodId
 * @property string $sPaymentMethodCode
 * @property string $iPaymentMethodName
 * @property integer $iPaymentGatewayId
 * @property integer $iHasChild
 * @property integer $bIsActive
 */
class PaymentMethod extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_mst_payment_method';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['iPaymentMethodId', 'iPaymentMethodName', 'iPaymentGatewayId'], 'required'],
            [['iPaymentMethodId', 'iPaymentGatewayId', 'iHasChild', 'bIsActive'], 'integer'],
            [['sPaymentMethodCode', 'iPaymentMethodName'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'iPaymentMethodId' => 'I Payment Method ID',
            'sPaymentMethodCode' => 'S Payment Method Code',
            'iPaymentMethodName' => 'I Payment Method Name',
            'iPaymentGatewayId' => 'I Payment Gateway ID',
            'iHasChild' => 'I Has Child',
            'bIsActive' => 'B Is Active',
        ];
    }
}
