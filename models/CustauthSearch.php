<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Custauth;

/**
 * CustauthSearch represents the model behind the search form about `app\models\Custauth`.
 */
class CustauthSearch extends Custauth
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'bIsACPreferred', 'iDeviceType', 'bIsMobileVerified', 'bIsEmailVerified'], 'integer'],
            [['mobile', 'sMobileNo2', 'password', 'name', 'email', 'sEmailId2', 'sClientStatusCode', 'model', 'deviceId', 'tGCMRegistrationNo', 'version', 'networkOperator', 'createddate', 'lastupdate', 'sMobileVerificationCode', 'dMobileVerificationTime', 'sCountryCode'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Custauth::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'bIsACPreferred' => $this->bIsACPreferred,
            'iDeviceType' => $this->iDeviceType,
            'createddate' => $this->createddate,
            'lastupdate' => $this->lastupdate,
            'bIsMobileVerified' => $this->bIsMobileVerified,
            'dMobileVerificationTime' => $this->dMobileVerificationTime,
            'bIsEmailVerified' => $this->bIsEmailVerified,
        ]);

        $query->andFilterWhere(['like', 'mobile', $this->mobile])
            ->andFilterWhere(['like', 'sMobileNo2', $this->sMobileNo2])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'sEmailId2', $this->sEmailId2])
            ->andFilterWhere(['like', 'sClientStatusCode', $this->sClientStatusCode])
            ->andFilterWhere(['like', 'model', $this->model])
            ->andFilterWhere(['like', 'deviceId', $this->deviceId])
            ->andFilterWhere(['like', 'tGCMRegistrationNo', $this->tGCMRegistrationNo])
            ->andFilterWhere(['like', 'version', $this->version])
            ->andFilterWhere(['like', 'networkOperator', $this->networkOperator])
            ->andFilterWhere(['like', 'sMobileVerificationCode', $this->sMobileVerificationCode])
            ->andFilterWhere(['like', 'sCountryCode', $this->sCountryCode]);

        return $dataProvider;
    }
}
