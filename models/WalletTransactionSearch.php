<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\WalletTransaction;

/**
 * WalletTransactionSearch represents the model behind the search form about `app\models\WalletTransaction`.
 */
class WalletTransactionSearch extends WalletTransaction
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['iRawId', 'iWalletId', 'iEntityRoleId', 'iPaymentMethodId', 'iPaymentStatusId', 'iPaymentGatewayId', 'iDeviceRegistrationId'], 'integer'],
            [['sEntityCode', 'sTransactionType', 'sCurrencyCode', 'nBalanceUpdateTime', 'sIntegrationReferenceId', 'sIntegrationRequest', 'sIntegrationResponse', 'sRemarks', 'sDeviceOs', 'sDeviceOsVersion', 'sDeviceMake', 'sDeviceModel', 'sApplicationType', 'sApplicationVersion', 'dCreatedDateTime', 'dModifiedDateTime'], 'safe'],
            [['nTransactionAmount', 'nBalanceBeforeTransaction', 'nBalanceAfterTransaction'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = WalletTransaction::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'iRawId' => $this->iRawId,
            'iWalletId' => $this->iWalletId,
            'iEntityRoleId' => $this->iEntityRoleId,
            'nTransactionAmount' => $this->nTransactionAmount,
            'nBalanceBeforeTransaction' => $this->nBalanceBeforeTransaction,
            'nBalanceAfterTransaction' => $this->nBalanceAfterTransaction,
            'nBalanceUpdateTime' => $this->nBalanceUpdateTime,
            'iPaymentMethodId' => $this->iPaymentMethodId,
            'iPaymentStatusId' => $this->iPaymentStatusId,
            'iPaymentGatewayId' => $this->iPaymentGatewayId,
            'iDeviceRegistrationId' => $this->iDeviceRegistrationId,
            'dCreatedDateTime' => $this->dCreatedDateTime,
            'dModifiedDateTime' => $this->dModifiedDateTime,
        ]);

        $query->andFilterWhere(['like', 'sEntityCode', $this->sEntityCode])
            ->andFilterWhere(['like', 'sTransactionType', $this->sTransactionType])
            ->andFilterWhere(['like', 'sCurrencyCode', $this->sCurrencyCode])
            ->andFilterWhere(['like', 'sIntegrationReferenceId', $this->sIntegrationReferenceId])
            ->andFilterWhere(['like', 'sIntegrationRequest', $this->sIntegrationRequest])
            ->andFilterWhere(['like', 'sIntegrationResponse', $this->sIntegrationResponse])
            ->andFilterWhere(['like', 'sRemarks', $this->sRemarks])
            ->andFilterWhere(['like', 'sDeviceOs', $this->sDeviceOs])
            ->andFilterWhere(['like', 'sDeviceOsVersion', $this->sDeviceOsVersion])
            ->andFilterWhere(['like', 'sDeviceMake', $this->sDeviceMake])
            ->andFilterWhere(['like', 'sDeviceModel', $this->sDeviceModel])
            ->andFilterWhere(['like', 'sApplicationType', $this->sApplicationType])
            ->andFilterWhere(['like', 'sApplicationVersion', $this->sApplicationVersion]);

        return $dataProvider;
    }
}
