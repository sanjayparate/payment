<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PaymentGateway;

/**
 * PaymentGatewaySearch represents the model behind the search form about `app\models\PaymentGateway`.
 */
class PaymentGatewaySearch extends PaymentGateway
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['iPaymentGatewayId', 'bIsActive'], 'integer'],
            [['sPaymentGatewayName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PaymentGateway::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'iPaymentGatewayId' => $this->iPaymentGatewayId,
            'bIsActive' => $this->bIsActive,
        ]);

        $query->andFilterWhere(['like', 'sPaymentGatewayName', $this->sPaymentGatewayName]);

        return $dataProvider;
    }
}
