<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_mst_currency_paymentmethod_gateway".
 *
 * @property integer $iRawId
 * @property string $sCurrencyCode
 * @property integer $iPaymentMethodId
 * @property integer $iPaymentGatewayId
 * @property integer $bIsActive
 * @property string $sEntityCode
 */
class PaymentMethodGateway extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_mst_currency_paymentmethod_gateway';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sCurrencyCode', 'iPaymentMethodId', 'iPaymentGatewayId'], 'required'],
            [['iPaymentMethodId', 'iPaymentGatewayId', 'bIsActive'], 'integer'],
            [['sCurrencyCode'], 'string', 'max' => 5],
            [['sEntityCode'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'iRawId' => 'I Raw ID',
            'sCurrencyCode' => 'S Currency Code',
            'iPaymentMethodId' => 'I Payment Method ID',
            'iPaymentGatewayId' => 'I Payment Gateway ID',
            'bIsActive' => 'B Is Active',
            'sEntityCode' => 'S Entity Code',
        ];
    }
}
