<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_wallet_transaction".
 *
 * @property integer $iRawId
 * @property integer $iWalletId
 * @property integer $iEntityRoleId
 * @property string $sEntityCode
 * @property string $sTransactionType
 * @property string $nTransactionAmount
 * @property string $sCurrencyCode
 * @property string $nBalanceBeforeTransaction
 * @property string $nBalanceAfterTransaction
 * @property string $nBalanceUpdateTime
 * @property integer $iPaymentMethodId
 * @property string $sIntegrationReferenceId
 * @property string $sIntegrationRequest
 * @property string $sIntegrationResponse
 * @property integer $iPaymentStatusId
 * @property string $sRemarks
 * @property integer $iPaymentGatewayId
 * @property integer $iDeviceRegistrationId
 * @property string $sDeviceOs
 * @property string $sDeviceOsVersion
 * @property string $sDeviceMake
 * @property string $sDeviceModel
 * @property string $sApplicationType
 * @property string $sApplicationVersion
 * @property string $dCreatedDateTime
 * @property string $dModifiedDateTime
 */
class WalletTransaction extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_wallet_transaction';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['iRawId', 'iWalletId', 'iEntityRoleId', 'sEntityCode', 'nTransactionAmount', 'sCurrencyCode', 'nBalanceBeforeTransaction', 'nBalanceAfterTransaction'], 'required'],
            [['iRawId', 'iWalletId', 'iEntityRoleId', 'iPaymentMethodId', 'iPaymentStatusId', 'iPaymentGatewayId', 'iDeviceRegistrationId'], 'integer'],
            [['nTransactionAmount', 'nBalanceBeforeTransaction', 'nBalanceAfterTransaction'], 'number'],
            [['nBalanceUpdateTime', 'dCreatedDateTime', 'dModifiedDateTime'], 'safe'],
            [['sIntegrationRequest', 'sIntegrationResponse', 'sRemarks'], 'string'],
            [['sEntityCode'], 'string', 'max' => 4],
            [['sTransactionType', 'sApplicationType'], 'string', 'max' => 15],
            [['sCurrencyCode'], 'string', 'max' => 5],
            [['sIntegrationReferenceId'], 'string', 'max' => 32],
            [['sDeviceOs', 'sDeviceOsVersion', 'sApplicationVersion'], 'string', 'max' => 10],
            [['sDeviceMake', 'sDeviceModel'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'iRawId' => 'I Raw ID',
            'iWalletId' => 'I Wallet ID',
            'iEntityRoleId' => 'I Entity Role ID',
            'sEntityCode' => 'S Entity Code',
            'sTransactionType' => 'S Transaction Type',
            'nTransactionAmount' => 'N Transaction Amount',
            'sCurrencyCode' => 'S Currency Code',
            'nBalanceBeforeTransaction' => 'N Balance Before Transaction',
            'nBalanceAfterTransaction' => 'N Balance After Transaction',
            'nBalanceUpdateTime' => 'N Balance Update Time',
            'iPaymentMethodId' => 'I Payment Method ID',
            'sIntegrationReferenceId' => 'S Integration Reference ID',
            'sIntegrationRequest' => 'S Integration Request',
            'sIntegrationResponse' => 'S Integration Response',
            'iPaymentStatusId' => 'I Payment Status ID',
            'sRemarks' => 'S Remarks',
            'iPaymentGatewayId' => 'I Payment Gateway ID',
            'iDeviceRegistrationId' => 'I Device Registration ID',
            'sDeviceOs' => 'S Device Os',
            'sDeviceOsVersion' => 'S Device Os Version',
            'sDeviceMake' => 'S Device Make',
            'sDeviceModel' => 'S Device Model',
            'sApplicationType' => 'S Application Type',
            'sApplicationVersion' => 'S Application Version',
            'dCreatedDateTime' => 'D Created Date Time',
            'dModifiedDateTime' => 'D Modified Date Time',
        ];
    }
}
