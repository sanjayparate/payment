<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "driver".
 *
 * @property integer $iSubscriberId
 * @property string $mobile
 * @property string $sPrimaryMobileNo
 * @property integer $iCabOperatorId
 * @property integer $iCityId
 * @property string $vehicle_registration_no
 * @property string $driver_name
 * @property integer $iDestinationLocId
 * @property integer $iDestinationZoneId
 * @property double $iDestinationLat
 * @property double $iDestinationLong
 * @property string $end_time
 * @property double $latitude
 * @property double $longitude
 * @property string $sTowerAddress
 * @property double $nDistanceFromNearestLocation
 * @property double $location_id
 * @property double $zone_id
 * @property string $cell_id_update_time
 * @property integer $self_update
 * @property string $time_self_update
 * @property double $balance
 * @property double $credit_limit
 * @property integer $blocked
 * @property string $balance_update_datetime
 * @property string $update_method
 * @property string $last_update_time
 * @property integer $iFareMeterStatus
 * @property integer $iTodayRevenue
 * @property integer $iDailyTargetRevenue
 * @property integer $iTodayDeficitRevenue
 * @property integer $iTodayTripCount
 * @property integer $iDailyTargetTripCount
 * @property integer $iTodayDeficitTripCount
 * @property integer $bIsIDEACallAPIToBeUsed
 * @property integer $bIsLockedForOBDType1Call
 * @property string $vehicle_type
 * @property integer $iVehicleTypeId
 * @property string $sVehicleModelCode
 * @property string $device_tag
 * @property integer $bHasVehicleCarrier
 * @property integer $bIsACVehicle
 * @property integer $bPrefForNonACTrip
 * @property string $owner_phone
 * @property string $taxi_stand
 * @property integer $rating
 * @property string $remarks
 * @property integer $bIsNightDriver
 * @property string $tShiftFromTime
 * @property string $tshiftToTime
 * @property integer $iAcquirerUserId
 * @property string $dDateOfAcquisition
 * @property string $sPermitHolderName
 * @property string $sPermitNumber
 * @property string $dDateOfRegistration
 * @property string $dLastSettlementDate
 * @property integer $bIsDisplay
 * @property integer $bIsDeleted
 * @property string $sDeleteReason
 * @property integer $iCreatedUserId
 * @property string $dCreatedDateTime
 * @property integer $iLastModifiedUserId
 * @property string $dLastModifiedDateTime
 * @property integer $iDeleteUserId
 * @property string $dDeleteDateTime
 */
class Driver extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'driver';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mobile', 'driver_name', 'iDestinationLocId', 'iDestinationZoneId', 'iDestinationLat', 'iDestinationLong', 'end_time', 'latitude', 'longitude', 'sTowerAddress', 'nDistanceFromNearestLocation', 'location_id', 'zone_id', 'cell_id_update_time', 'self_update', 'time_self_update', 'balance_update_datetime', 'update_method', 'last_update_time', 'vehicle_type', 'iVehicleTypeId', 'sVehicleModelCode', 'device_tag', 'owner_phone', 'taxi_stand', 'remarks', 'tShiftFromTime', 'tshiftToTime', 'sPermitHolderName', 'sPermitNumber', 'dDateOfRegistration', 'dLastSettlementDate', 'iCreatedUserId', 'iLastModifiedUserId', 'dLastModifiedDateTime', 'iDeleteUserId', 'dDeleteDateTime'], 'required'],
            [['iCabOperatorId', 'iCityId', 'iDestinationLocId', 'iDestinationZoneId', 'self_update', 'blocked', 'iFareMeterStatus', 'iTodayRevenue', 'iDailyTargetRevenue', 'iTodayDeficitRevenue', 'iTodayTripCount', 'iDailyTargetTripCount', 'iTodayDeficitTripCount', 'bIsIDEACallAPIToBeUsed', 'bIsLockedForOBDType1Call', 'iVehicleTypeId', 'bHasVehicleCarrier', 'bIsACVehicle', 'bPrefForNonACTrip', 'rating', 'bIsNightDriver', 'iAcquirerUserId', 'bIsDisplay', 'bIsDeleted', 'iCreatedUserId', 'iLastModifiedUserId', 'iDeleteUserId'], 'integer'],
            [['iDestinationLat', 'iDestinationLong', 'latitude', 'longitude', 'nDistanceFromNearestLocation', 'location_id', 'zone_id', 'balance', 'credit_limit'], 'number'],
            [['end_time', 'cell_id_update_time', 'time_self_update', 'balance_update_datetime', 'last_update_time', 'tShiftFromTime', 'tshiftToTime', 'dDateOfAcquisition', 'dDateOfRegistration', 'dLastSettlementDate', 'dCreatedDateTime', 'dLastModifiedDateTime', 'dDeleteDateTime'], 'safe'],
            [['mobile', 'sPrimaryMobileNo', 'owner_phone'], 'string', 'max' => 12],
            [['vehicle_registration_no', 'update_method', 'vehicle_type', 'device_tag', 'taxi_stand', 'sPermitNumber'], 'string', 'max' => 20],
            [['driver_name', 'remarks', 'sPermitHolderName'], 'string', 'max' => 100],
            [['sTowerAddress'], 'string', 'max' => 500],
            [['sVehicleModelCode'], 'string', 'max' => 10],
            [['sDeleteReason'], 'string', 'max' => 1000],
            [['mobile'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'iSubscriberId' => 'I Subscriber ID',
            'mobile' => 'Mobile',
            'sPrimaryMobileNo' => 'S Primary Mobile No',
            'iCabOperatorId' => 'I Cab Operator ID',
            'iCityId' => 'I City ID',
            'vehicle_registration_no' => 'Vehicle Registration No',
            'driver_name' => 'Driver Name',
            'iDestinationLocId' => 'I Destination Loc ID',
            'iDestinationZoneId' => 'I Destination Zone ID',
            'iDestinationLat' => 'I Destination Lat',
            'iDestinationLong' => 'I Destination Long',
            'end_time' => 'End Time',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'sTowerAddress' => 'S Tower Address',
            'nDistanceFromNearestLocation' => 'N Distance From Nearest Location',
            'location_id' => 'Location ID',
            'zone_id' => 'Zone ID',
            'cell_id_update_time' => 'Cell Id Update Time',
            'self_update' => 'Self Update',
            'time_self_update' => 'Time Self Update',
            'balance' => 'Balance',
            'credit_limit' => 'Credit Limit',
            'blocked' => 'Blocked',
            'balance_update_datetime' => 'Balance Update Datetime',
            'update_method' => 'Update Method',
            'last_update_time' => 'Last Update Time',
            'iFareMeterStatus' => 'I Fare Meter Status',
            'iTodayRevenue' => 'I Today Revenue',
            'iDailyTargetRevenue' => 'I Daily Target Revenue',
            'iTodayDeficitRevenue' => 'I Today Deficit Revenue',
            'iTodayTripCount' => 'I Today Trip Count',
            'iDailyTargetTripCount' => 'I Daily Target Trip Count',
            'iTodayDeficitTripCount' => 'I Today Deficit Trip Count',
            'bIsIDEACallAPIToBeUsed' => 'B Is Ideacall Apito Be Used',
            'bIsLockedForOBDType1Call' => 'B Is Locked For Obdtype1 Call',
            'vehicle_type' => 'Vehicle Type',
            'iVehicleTypeId' => 'I Vehicle Type ID',
            'sVehicleModelCode' => 'S Vehicle Model Code',
            'device_tag' => 'Device Tag',
            'bHasVehicleCarrier' => 'B Has Vehicle Carrier',
            'bIsACVehicle' => 'B Is Acvehicle',
            'bPrefForNonACTrip' => 'B Pref For Non Actrip',
            'owner_phone' => 'Owner Phone',
            'taxi_stand' => 'Taxi Stand',
            'rating' => 'Rating',
            'remarks' => 'Remarks',
            'bIsNightDriver' => 'B Is Night Driver',
            'tShiftFromTime' => 'T Shift From Time',
            'tshiftToTime' => 'Tshift To Time',
            'iAcquirerUserId' => 'I Acquirer User ID',
            'dDateOfAcquisition' => 'D Date Of Acquisition',
            'sPermitHolderName' => 'S Permit Holder Name',
            'sPermitNumber' => 'S Permit Number',
            'dDateOfRegistration' => 'D Date Of Registration',
            'dLastSettlementDate' => 'D Last Settlement Date',
            'bIsDisplay' => 'B Is Display',
            'bIsDeleted' => 'B Is Deleted',
            'sDeleteReason' => 'S Delete Reason',
            'iCreatedUserId' => 'I Created User ID',
            'dCreatedDateTime' => 'D Created Date Time',
            'iLastModifiedUserId' => 'I Last Modified User ID',
            'dLastModifiedDateTime' => 'D Last Modified Date Time',
            'iDeleteUserId' => 'I Delete User ID',
            'dDeleteDateTime' => 'D Delete Date Time',
        ];
    }
}
