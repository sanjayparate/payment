<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_mst_payment_status".
 *
 * @property integer $iPaymentStatusId
 * @property integer $bIsSuccess
 * @property string $sStatusCode
 * @property string $sStatusName
 * @property integer $iPaymentGatewayId
 */
class PaymentStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_mst_payment_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bIsSuccess', 'iPaymentGatewayId'], 'integer'],
            [['sStatusCode', 'sStatusName', 'iPaymentGatewayId'], 'required'],
            [['sStatusCode'], 'string', 'max' => 32],
            [['sStatusName'], 'string', 'max' => 15],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'iPaymentStatusId' => 'I Payment Status ID',
            'bIsSuccess' => 'B Is Success',
            'sStatusCode' => 'S Status Code',
            'sStatusName' => 'S Status Name',
            'iPaymentGatewayId' => 'I Payment Gateway ID',
        ];
    }
}
