<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Wallet;

/**
 * WalletSearch represents the model behind the search form about `app\models\Wallet`.
 */
class WalletSearch extends Wallet
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['iWalletId', 'iEntityRoleId', 'bIsActive'], 'integer'],
            [['sEntityCode', 'sCurrencyCode', 'dCreatedDateTime', 'dModifiedDateTime'], 'safe'],
            [['nBalance'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Wallet::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'iWalletId' => $this->iWalletId,
            'iEntityRoleId' => $this->iEntityRoleId,
            'nBalance' => $this->nBalance,
            'bIsActive' => $this->bIsActive,
            'dCreatedDateTime' => $this->dCreatedDateTime,
            'dModifiedDateTime' => $this->dModifiedDateTime,
        ]);

        $query->andFilterWhere(['like', 'sEntityCode', $this->sEntityCode])
            ->andFilterWhere(['like', 'sCurrencyCode', $this->sCurrencyCode]);

        return $dataProvider;
    }
}
