<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_mst_entity_role".
 *
 * @property integer $iEntityRoleId
 * @property string $sEntityCode
 * @property string $sEntityName
 */
class EntityRole extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_mst_entity_role';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['iEntityRoleId', 'sEntityCode'], 'required'],
            [['iEntityRoleId'], 'integer'],
            [['sEntityCode'], 'string', 'max' => 4],
            [['sEntityName'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'iEntityRoleId' => 'I Entity Role ID',
            'sEntityCode' => 'S Entity Code',
            'sEntityName' => 'S Entity Name',
        ];
    }
}
