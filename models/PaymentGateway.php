<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_mst_payment_gateway".
 *
 * @property integer $iPaymentGatewayId
 * @property string $sPaymentGatewayName
 * @property integer $bIsActive
 */
class PaymentGateway extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_mst_payment_gateway';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sPaymentGatewayName'], 'required'],
            [['bIsActive'], 'integer'],
            [['sPaymentGatewayName'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'iPaymentGatewayId' => 'I Payment Gateway ID',
            'sPaymentGatewayName' => 'S Payment Gateway Name',
            'bIsActive' => 'B Is Active',
        ];
    }
}
