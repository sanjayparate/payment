<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Driver;

/**
 * DriverSearch represents the model behind the search form about `app\models\Driver`.
 */
class DriverSearch extends Driver
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['iSubscriberId', 'iCabOperatorId', 'iCityId', 'iDestinationLocId', 'iDestinationZoneId', 'self_update', 'blocked', 'iFareMeterStatus', 'iTodayRevenue', 'iDailyTargetRevenue', 'iTodayDeficitRevenue', 'iTodayTripCount', 'iDailyTargetTripCount', 'iTodayDeficitTripCount', 'bIsIDEACallAPIToBeUsed', 'bIsLockedForOBDType1Call', 'iVehicleTypeId', 'bHasVehicleCarrier', 'bIsACVehicle', 'bPrefForNonACTrip', 'rating', 'bIsNightDriver', 'iAcquirerUserId', 'bIsDisplay', 'bIsDeleted', 'iCreatedUserId', 'iLastModifiedUserId', 'iDeleteUserId'], 'integer'],
            [['mobile', 'sPrimaryMobileNo', 'vehicle_registration_no', 'driver_name', 'end_time', 'sTowerAddress', 'cell_id_update_time', 'time_self_update', 'balance_update_datetime', 'update_method', 'last_update_time', 'vehicle_type', 'sVehicleModelCode', 'device_tag', 'owner_phone', 'taxi_stand', 'remarks', 'tShiftFromTime', 'tshiftToTime', 'dDateOfAcquisition', 'sPermitHolderName', 'sPermitNumber', 'dDateOfRegistration', 'dLastSettlementDate', 'sDeleteReason', 'dCreatedDateTime', 'dLastModifiedDateTime', 'dDeleteDateTime'], 'safe'],
            [['iDestinationLat', 'iDestinationLong', 'latitude', 'longitude', 'nDistanceFromNearestLocation', 'location_id', 'zone_id', 'balance', 'credit_limit'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Driver::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'iSubscriberId' => $this->iSubscriberId,
            'iCabOperatorId' => $this->iCabOperatorId,
            'iCityId' => $this->iCityId,
            'iDestinationLocId' => $this->iDestinationLocId,
            'iDestinationZoneId' => $this->iDestinationZoneId,
            'iDestinationLat' => $this->iDestinationLat,
            'iDestinationLong' => $this->iDestinationLong,
            'end_time' => $this->end_time,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'nDistanceFromNearestLocation' => $this->nDistanceFromNearestLocation,
            'location_id' => $this->location_id,
            'zone_id' => $this->zone_id,
            'cell_id_update_time' => $this->cell_id_update_time,
            'self_update' => $this->self_update,
            'time_self_update' => $this->time_self_update,
            'balance' => $this->balance,
            'credit_limit' => $this->credit_limit,
            'blocked' => $this->blocked,
            'balance_update_datetime' => $this->balance_update_datetime,
            'last_update_time' => $this->last_update_time,
            'iFareMeterStatus' => $this->iFareMeterStatus,
            'iTodayRevenue' => $this->iTodayRevenue,
            'iDailyTargetRevenue' => $this->iDailyTargetRevenue,
            'iTodayDeficitRevenue' => $this->iTodayDeficitRevenue,
            'iTodayTripCount' => $this->iTodayTripCount,
            'iDailyTargetTripCount' => $this->iDailyTargetTripCount,
            'iTodayDeficitTripCount' => $this->iTodayDeficitTripCount,
            'bIsIDEACallAPIToBeUsed' => $this->bIsIDEACallAPIToBeUsed,
            'bIsLockedForOBDType1Call' => $this->bIsLockedForOBDType1Call,
            'iVehicleTypeId' => $this->iVehicleTypeId,
            'bHasVehicleCarrier' => $this->bHasVehicleCarrier,
            'bIsACVehicle' => $this->bIsACVehicle,
            'bPrefForNonACTrip' => $this->bPrefForNonACTrip,
            'rating' => $this->rating,
            'bIsNightDriver' => $this->bIsNightDriver,
            'tShiftFromTime' => $this->tShiftFromTime,
            'tshiftToTime' => $this->tshiftToTime,
            'iAcquirerUserId' => $this->iAcquirerUserId,
            'dDateOfAcquisition' => $this->dDateOfAcquisition,
            'dDateOfRegistration' => $this->dDateOfRegistration,
            'dLastSettlementDate' => $this->dLastSettlementDate,
            'bIsDisplay' => $this->bIsDisplay,
            'bIsDeleted' => $this->bIsDeleted,
            'iCreatedUserId' => $this->iCreatedUserId,
            'dCreatedDateTime' => $this->dCreatedDateTime,
            'iLastModifiedUserId' => $this->iLastModifiedUserId,
            'dLastModifiedDateTime' => $this->dLastModifiedDateTime,
            'iDeleteUserId' => $this->iDeleteUserId,
            'dDeleteDateTime' => $this->dDeleteDateTime,
        ]);

        $query->andFilterWhere(['like', 'mobile', $this->mobile])
            ->andFilterWhere(['like', 'sPrimaryMobileNo', $this->sPrimaryMobileNo])
            ->andFilterWhere(['like', 'vehicle_registration_no', $this->vehicle_registration_no])
            ->andFilterWhere(['like', 'driver_name', $this->driver_name])
            ->andFilterWhere(['like', 'sTowerAddress', $this->sTowerAddress])
            ->andFilterWhere(['like', 'update_method', $this->update_method])
            ->andFilterWhere(['like', 'vehicle_type', $this->vehicle_type])
            ->andFilterWhere(['like', 'sVehicleModelCode', $this->sVehicleModelCode])
            ->andFilterWhere(['like', 'device_tag', $this->device_tag])
            ->andFilterWhere(['like', 'owner_phone', $this->owner_phone])
            ->andFilterWhere(['like', 'taxi_stand', $this->taxi_stand])
            ->andFilterWhere(['like', 'remarks', $this->remarks])
            ->andFilterWhere(['like', 'sPermitHolderName', $this->sPermitHolderName])
            ->andFilterWhere(['like', 'sPermitNumber', $this->sPermitNumber])
            ->andFilterWhere(['like', 'sDeleteReason', $this->sDeleteReason]);

        return $dataProvider;
    }
}
